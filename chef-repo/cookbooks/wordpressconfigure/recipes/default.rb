#
# Cookbook Name:: wordpressconfigure
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Use wp-config.php file from repo that is preconfigured, placing in wordpress directory allowing you to bypass the inital web ui setup
template "/var/www/wordpress/wp-config.php" do
  source "wp-config.php.erb"
  mode "0644"
end

# For the apache2 (httpd) service, restart it so the necessary file changes take effect
service 'apache2' do
  action [ :restart ]
end
