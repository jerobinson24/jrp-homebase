#!/bin/bash

#Install chef-client
#knife bootstrap 184.173.17.90 -x root -P M365jsLz --sudo -N mywordpress1

#Change title of blog page
sed -i 's#<title>New Blog Title</title>#<title>SoftLayer Test Title</title>#g' cookbooks/pageadd/templates/default/index.html.erb

#Upload changed cookbook
knife cookbook upload pageadd

#Now run chef-client on the node for the change to take effect
sshpass -p M365jsLz ssh root@184.173.17.90 chef-client
#Another option
#knife ssh name:mywordpress1 -x root -P M365jsLz "chef-client" -a 184.173.17.90
