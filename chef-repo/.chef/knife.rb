# See http://docs.getchef.com/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "jerobinson24"
client_key               "#{current_dir}/jerobinson24.pem"
validation_client_name   "jrp-validator"
validation_key           "#{current_dir}/jrp-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/jrp"
cache_type               'BasicFile'
cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
cookbook_path            ["#{current_dir}/../cookbooks"]
