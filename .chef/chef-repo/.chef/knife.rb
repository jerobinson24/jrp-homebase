log_level                :info
log_location             STDOUT
node_name                'jerobinson24'
client_key               '/root/jrp-homebase/.chef/chef-repo/.chef/jerobinson24.pem'
validation_client_name   'jrp-validator'
validation_key           '/etc/chef-server/chef-validator.pem'
chef_server_url          'https://jrp.mydomain.com:443'
syntax_check_cache_path  '/root/jrp-homebase/.chef/chef-repo/.chef/syntax_check_cache'
